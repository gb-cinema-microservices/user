module gb-cinema-user

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jackc/pgx/v4 v4.11.0
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20210406143921-e86de6bf7a46 // indirect
	google.golang.org/grpc v1.37.0 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
