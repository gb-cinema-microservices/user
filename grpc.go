package main

import (
	"context"
	pb "gb-cinema-user/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Service struct {
}

func (s *Service) SearchUserByToken(ctx context.Context, req *pb.UserToken) (resp *pb.User, err error) {
	const sql = `SELECT id, email, name, paid, pwd, token FROM users WHERE token LIKE $1 LIMIT 1;`
	resp = &pb.User{}
	err = Postgres.QueryRow(ctx, sql, req.Token).Scan(
		&resp.UserId,
		&resp.Email,
		&resp.Name,
		&resp.IsPaid,
		&resp.Pwd,
		&resp.Token)
	if err != nil {
		err = status.Errorf(codes.NotFound, "SQL Error: %v", err)
	}
	return resp, err
}

func (s *Service) SearchUserByEmailAndPassword(ctx context.Context, req *pb.UserEmailAndPassword) (resp *pb.User, err error) {
	const sql = `SELECT id, email, name, paid, pwd, token FROM users WHERE email LIKE $1 AND pwd LIKE $2 LIMIT 1;`
	resp = &pb.User{}
	err = Postgres.QueryRow(ctx, sql, req.Email, req.Pwd).Scan(
		&resp.UserId,
		&resp.Email,
		&resp.Name,
		&resp.IsPaid,
		&resp.Pwd,
		&resp.Token)
	if err != nil {
		err = status.Errorf(codes.NotFound, "SQL Error: %v", err)
	}
	return resp, err
}

func (s *Service) PatchUserPaid(ctx context.Context, req *pb.UserIdAndIspaid) (resp *pb.User, err error) {
	const sql = `UPDATE users SET paid = $2 WHERE id = $1 RETURNING id, email, name, paid, pwd, token;`
	resp = &pb.User{}
	err = Postgres.QueryRow(ctx, sql, req.UserId, req.IsPaid).Scan(
		&resp.UserId,
		&resp.Email,
		&resp.Name,
		&resp.IsPaid,
		&resp.Pwd,
		&resp.Token)
	if err != nil {
		err = status.Errorf(codes.NotFound, "SQL Error: %v", err)
	}
	return resp, err
}
