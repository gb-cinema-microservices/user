package main

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
)

var Postgres *pgxpool.Pool

func ConnectPostgres(ctx context.Context, url string) *pgxpool.Pool {
	dbpool, err := pgxpool.Connect(ctx, url)
	if err != nil {
		panic(err)
	}
	return dbpool
}
