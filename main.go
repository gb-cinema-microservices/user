package main

import (
	"context"
	pb "gb-cinema-user/api"
	"google.golang.org/grpc"
	"log"
	"net"
)

const DatabaseAddr = "postgres://usr:secret@elpy.bmrng.net:5432/usr"
const ListenAddr = ":8082"

func main() {
	// DB:
	ctx := context.Background()
	Postgres = ConnectPostgres(ctx, DatabaseAddr)
	defer Postgres.Close()

	// gRPC API:
	rpc := grpc.NewServer()
	pb.RegisterUserStorageServer(rpc, &Service{})
	listener, err := net.Listen("tcp", ListenAddr)
	if err != nil {
		log.Fatalf("Failed to listen TCP port: %v", err)
	}
	log.Println("Starting gRPC server on", ListenAddr)
	err = rpc.Serve(listener)
	if err != nil {
		log.Fatalf("Failed to serve gRPC: %v", err)
	}
}
